#![feature(proc_macro_hygiene, decl_macro)]

mod parse;
mod render;

#[macro_use]
extern crate rocket;

#[macro_use]
extern crate serde_json;

use rocket::response::content::Html;
use serde::Serialize;

#[derive(Debug, Serialize)]
struct Items(Vec<Item>);

#[derive(Debug, Serialize)]
struct Item {
    name: String,
    notes: Option<String>,
    sub_items: Option<Items>,
}

#[get("/")]
fn index() -> Html<String> {
    Html(dbg!(Items::serialize(include_str!("../../notes.yaml"))).render())
}

fn main() {
    rocket::ignite().mount("/", routes![index]).launch();
}
